#include<vector>
#include<SFML/Graphics.hpp>
#include "Point.hpp" 
#pragma once


class GameOfLife {

private:
    std::vector<std::vector<Point*>>pointGrid ;
    int diff ; 
public:
    GameOfLife();
    GameOfLife(const std::vector<std::vector<int>> &grid ,int diff );
    ~GameOfLife();
public:
    void assign(const std::vector<std::vector<int>> &grid);
    void Paint(sf::RenderWindow &window);
    int Getneighbours(Point * );
    void nextGen();

};