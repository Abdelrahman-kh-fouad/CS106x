#include "../include/GameOfLife.hpp"

#include<iostream>
GameOfLife::GameOfLife() {
    
}
GameOfLife::GameOfLife(const std::vector<std::vector<int>>&grid , int diff)
{
    this->diff = diff; 
    this->assign(grid);
}

void GameOfLife::assign(const std::vector<std::vector<int>>&grid)
{
    if(grid.size() != this->pointGrid.size() || grid.front().size() != this->pointGrid.front().size() )
        this->pointGrid.assign(grid.size() , std::vector<Point*>(grid.front().size() , nullptr));

    for(int i =0 ;i < grid.size() ;i++ )
    {
        for(int j =0 ;j < grid.front().size() ; j++ )
        {
            this->pointGrid[i][j] = new Point(i, j , grid[i][j]  ,diff);
        }
    
    }
    std::cout << "dr"<<std::endl;
}

void GameOfLife::Paint(sf::RenderWindow &window)
{
    for(std::vector<Point*> &i :this->pointGrid)
    {
        for(Point* j :i )
        {
            j->Draw(window);
        }
    }
}

int GameOfLife::Getneighbours(Point *p )
{
    int xx[] = {0 , 0 , 1 , -1 , 1 , 1 , -1 ,-1 };
    int yy[] = {1 , -1 , 0 , 0 , -1 , 1  ,1 , -1 };
    int res =0 ; 
    for(int i =0 ;i < 8 ; i++ )
    {
        if(p->oX+xx[i] >=0 && p->oX+xx[i] < this->pointGrid.size() && p->oY+yy[i] >=0 && p->oY+yy[i] < this->pointGrid.front().size())
            res += this->pointGrid[p->oX + xx[i]][p->oY + yy[i]]->level; 
    }
    return res ;
}

void GameOfLife::nextGen()
{
    std::vector<std::vector<Point*>>nextPoints(this->pointGrid.size() ,std::vector<Point*>(this->pointGrid.front().size() , nullptr) );  
    for(int i = 0 ; i < this->pointGrid.size() ;i++)
    {
        for(int j =0 ;j < this->pointGrid.front().size() ;j++)
        {
            Point* current = this->pointGrid[i][j];
            Point* &next = nextPoints[i][j];
            next = new Point(current->oX , current->oY , current->level ,current->constDiff );

            int cntN = this->Getneighbours(current);
            if(cntN == 1 || 0 )
                next->level = 0 ;
            else if (cntN == 3 )
                next->level =1 ;
            else if (cntN >=4 )
                next->level = 0 ;

        }
    }

    this->pointGrid.assign(nextPoints.begin() , nextPoints.end()); 

}
GameOfLife::~GameOfLife() {

}