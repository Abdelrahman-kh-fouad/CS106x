
#include "../include/Read.hpp"

Read::Read() {
   
    std::cin >> this->n >>this->m ;
    this->gridChar.assign(this->n , std::vector<int>(this->m , 0 ));
    for(int i =0 ;i < this->n ;i++)
    {
        for(int j =0 ; j< this->m ;j++ )
        {
            char tmp ; 
            std::cin >>tmp; 
            this->gridChar[i][j] = (tmp == 'X')? 1 : 0 ; 
        }
    }
     
}

Read::~Read() {

}

std::vector<std::vector<int>>* Read::GetGrid()
{
    return &(this->gridChar);
}