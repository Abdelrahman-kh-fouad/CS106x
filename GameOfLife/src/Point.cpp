#include "../include/Point.hpp"
#include <iostream>
Point::Point(int x , int y  ,int level,int constDiff) {
    this->y = x * constDiff;
    this->x = y * constDiff;
    this->oX =x ; 
    this->oY =y ; 

    this->constDiff = constDiff;
    this->level = level;
    this->pointSF.setPosition(sf::Vector2f(this->x , this->y ));
    this->pointSF.setSize(sf::Vector2f(this->constDiff , this->constDiff));
}
void Point::Draw(sf::RenderWindow &windowWeWant)
{
    this->pointSF.setFillColor(sf::Color(sf::Uint8(255) , sf::Uint8(255) , sf::Uint8(255) ,  sf::Uint8(255* this->level)));
    windowWeWant.draw(this->pointSF);  
}

Point::~Point() {}

