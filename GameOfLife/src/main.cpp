#include <SFML/Graphics.hpp>
#include "../include/Read.hpp"
#include "../include/GameOfLife.hpp"

int main()
{
    Read file;

    std::vector<std::vector<int>>*res = file.GetGrid();
    GameOfLife game(*res , 10);
    sf::RenderWindow window(sf::VideoMode(file.m * 10 , file.n * 10), "Game Of Life");
    window.setFramerateLimit(5);
   
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        window.clear();
        game.Paint(window);
        game.nextGen();

        window.display();
    }

    return 0;
}