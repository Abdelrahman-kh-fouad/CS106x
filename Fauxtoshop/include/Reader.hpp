#include <iostream>
#include <string>
#include <SFML/Graphics.hpp>
#include "../include/Image.hpp"
#pragma once

class Reader {
private:
    int choice;
    Image* image ;

public:
    std::string filePath;
public:
    Reader(Image* &);
    ~Reader();

public:
    void Read();

};