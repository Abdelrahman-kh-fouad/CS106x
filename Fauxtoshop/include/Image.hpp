#include <SFML/Graphics.hpp>
#include <vector>
#include <string>

#pragma once
class Image{
public:
    Image();
    Image(std::string imageFile );
    ~Image();
private:
    std::vector<std::vector<std::vector<sf::Color>>>options;
public:
    int choice ;
public:
    void MakeScatter(int);
    void MakeEdged(int);
    std::vector<std::pair<std::pair<int ,int > , sf::Color>> CutGreen(int);
    void PutInPic(const std::vector<std::pair<std::pair<int,int> ,sf::Color >>& , std::pair<int,int>);
    int Compare(Image &other);
    void Draw(sf::RenderWindow &window  );
};