#include "../include/Reader.hpp"

Reader::Reader(Image* &image )
{
    std::cout<< "file Name : ";
    std::cin >> this->filePath;

    this->image= new Image(this->filePath );
    image = this -> image ;

}
void Reader::Read() 
{
    std::cin >> this->choice ;
    switch (choice) {
        case 1:
        {
            int degree;
            std::cout << "Enter degree : ";
            std::cin >> degree;
            this->image->MakeScatter(degree);
            this->image->choice = this->choice;
        }
        break;
        case 2 :
        {
            int threhold;
            std::cout << "Enter Threehold";
            std::cin >> threhold;
            this->image->MakeEdged(threhold);
            this->image->choice = this->choice;
        }
        break;
        case 3 :
        {
            int threhold;
            std::cout << "Enter threhold : ";
            std::cin >> threhold;
            std::cout << "Enter the image you want to copy : ";
            std::string imageToCutName;
            std::cin >> imageToCutName;
            std::pair<int, int> location;
            std::cout << "Enter location : \n x : ";
            std::cin >> location.first;
            std::cout << "y : ";
            std::cin >> location.second;
            Image imageToCut(imageToCutName);
            this->image->PutInPic(imageToCut.CutGreen(threhold), location);
            this->image->choice = this->choice;
        }
        break;
        case 4 :
        {
            std::string other;
            std::cout << "Enter another image : ";
            std::cin >> other;
            Image otherImage(other);
            std::cout << this->image->Compare(otherImage) << std::endl;
        }
        break;
        default:
            break;
    }
}

Reader::~Reader() {}