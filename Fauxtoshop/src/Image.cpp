#include "../include/Image.hpp"
Image::Image() {}
Image::Image(std::string imageFile)
{
    this->options.assign(7 , std::vector<std::vector<sf::Color>>());

    sf::Image image ;
    image.loadFromFile(imageFile);
    sf::Vector2u imageSize = image.getSize();
    for(auto &cur : this->options)
        cur.assign(imageSize.x , std::vector<sf::Color>(imageSize.y));

    std::vector<std::vector<sf::Color>> &pixels = this->options[0] ;
    for(int i =0 ;i < imageSize.x ;i++ )
    {
        for(int j =0 ;j < imageSize.y ; j++ )
        {
            pixels[i][j] = image.getPixel(i ,j );
        }
    }
    this->choice = 0 ;
}

void Image::MakeScatter(int degree)
{
    std::vector<std::vector<sf::Color>> &org = this->options[0];
    std::vector<std::vector<sf::Color>> &scatted = this->options[1];

    for(int i =0 ;i < org.size() ; i++)
    {
        for(int j =0 ;j < org.front().size();j++)
        {
            std::pair<int ,int >x , y ;
            x = std::make_pair(std::max(0 ,i - degree ) , std::min((int)org.size()-1 , i + degree));
            y = std::make_pair(std::max(0 ,j - degree ) , std::min((int)org.front().size()-1 , j + degree));
            std::pair<int ,int >predict = {i , j } ;
            while (predict == std::make_pair(i , j ))
                predict.first = x.first + std::rand() %(x.second -x.first +1 ) ,
                predict.second = y.first + std::rand() %(y.second - y.first +1 );
            scatted[i][j] = org[predict.first][predict.second];
        }
    }

}
void Image::MakeEdged(int threhold)
{
    std::vector<std::vector<sf::Color>> &org = this->options[0];
    std::vector<std::vector<sf::Color>> &edged = this->options[2];
    int xx[] = {0 ,0 ,1 ,-1 ,-1 ,-1 ,1 ,1 };
    int yy[] = {1, -1,0 ,0 , 1 , -1 ,1 ,-1 };
    for(int i =0 ;i < org.size() ;i++ )
    {
        for(int j =0 ;j < org.front().size() ;j++)
        {
            bool isEdge = 0 ;
            for(int k = 0; k < 8 ;k++ )
            {
                if(i + xx[k] >= 0 && i +xx[k] < org.size()
                && j + yy[k] >= 0 && j +yy[k] < org.front().size())
                    isEdge |= (std::abs(org[i][j].r - org[i+xx[k]][j+yy[k]].r)
                            + std::abs(org[i][j].b - org[i+xx[k]][j+yy[k]].b)
                            + std::abs(org[i][j].g - org[i+xx[k]][j+yy[k]].g)) > threhold;
            }

            if (isEdge)
                edged[i][j] = sf::Color::Black;
            else
                edged[i][j] = sf::Color::White;
        }
    }
}

std::vector<std::pair<std::pair<int ,int > , sf::Color>> Image::CutGreen(int tolerance)
{
    std::vector<std::vector<sf::Color>> &org = this->options[0];
    std::vector<std::pair<std::pair<int ,int > , sf::Color>>cut ;
    for(int i =0 ;i < org.size() ;i++)
    {
        for(int j =0 ;j < org.front().size() ;j++)
        {
            if(!((org[i][j].g && !org[i][j].r && !org[i][j].b) && (org[i][j].g /(float)255)*100 >= tolerance ))
                cut.push_back(std::make_pair(std::make_pair(i ,j ) , org[i][j]));
        }
    }
    return cut ;
}

void Image::PutInPic(const std::vector<std::pair<std::pair<int, int>, sf::Color>> &cut , std::pair<int ,int >location)
{   std::vector<std::vector<sf::Color>> &org = this->options[0];
    std::vector<std::vector<sf::Color>> &toBeAdded = this->options[3];
    toBeAdded.assign(org.begin() , org.end());
    for (auto &i : cut)
    {
        if(location.first + i.first.first < org.size() && location.second + i.first.second < org.front().size() )
            toBeAdded[i.first.first + location.first][i.first.second + location.second] = i.second;
    }
}


int Image::Compare(Image &other)
{
    std::vector<std::vector<sf::Color>>&orgImg = this->options[0];
    std::vector<std::vector<sf::Color>>&otherImg = other.options[0];
    int res =0 ;
    for(int i =0 ;i < orgImg.size() && i < otherImg.size(); i++)
    {
        for(int j = 0 ;j < orgImg.front().size() && j< otherImg.front().size() ;j++ )
        {
            if(orgImg[i][j] != otherImg[i][j])
                res++;
        }
    }
    return res ;
}


void Image::Draw(sf::RenderWindow &window)
{

    std::vector<std::vector<sf::Color>>&pixels = this->options[this->choice];
    sf::Image image;
    image.create((int)pixels.size() , (int)pixels.front().size());
    for(int i =0 ;i < pixels.size() ;i++)
    {
        for(int j =0 ; j < pixels.front().size() ;j++)
        {
            image.setPixel(i ,j , pixels[i][j]);
        }
    }
    sf::Texture texture;
    texture.loadFromImage(image);
    sf::Sprite sprite ;
    sprite.setTexture(texture , true);
    window.draw(sprite);
}
Image::~Image() {}