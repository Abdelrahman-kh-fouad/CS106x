#include <SFML/Graphics.hpp>
#include "../include/Image.hpp"
#include "../include/Reader.hpp"
int main()
{
    sf::RenderWindow window;
    Image *image = nullptr;
    Reader reader(image );
    window.create(sf::VideoMode(1200 , 1200) ,"shop");

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();
        image->Draw(window );
        window.display();
        reader.Read();

    }

    return 0;
}